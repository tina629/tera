/*
 * @project: TERA
 * @version: Development (beta)
 * @license: MIT (not for evil)
 * @copyright: Yuriy Ivanov 2017-2019 [progr76@gmail.com]
 * Web: https://terafoundation.org
 * Twitter: https://twitter.com/terafoundation
 * Telegram: https://web.telegram.org/#/im?p=@terafoundation
*/

var lastcoin = 1000000000;

function show()
{
    if(global.DApps && GENERATE_BLOCK_ACCOUNT)
    {
        var arr = DApps.Accounts.GetRowsAccounts(GENERATE_BLOCK_ACCOUNT, 1);
        var Data = arr[0];
        var sumcoin = Data.Value.SumCOIN;
        var delta = sumcoin - lastcoin;
        lastcoin = sumcoin;
        if(delta > 200)
        {
            ToLog("ID:" + GENERATE_BLOCK_ACCOUNT);
            if(global.COREY_WATCH_DOG === 1)
            {
                ToLog("Its forked  restart now");
                RestartNode();
            }
            else
                if(global.COREY_WATCH_DOG === 2)
                {
                    SERVER.FREE_ALL_MEM_CHAINS();
                    var Num = SERVER.BlockNumDB - 5000;
                    if(Num < 100)
                        Num = 100;
                    lastcoin = 1000000000;
                    ToLog("Its forked - truncate now from block: " + Num);
                    SERVER.SetTruncateBlockDB(Num);
                }
        }
    }
};
if(global.COREY_WATCH_DOG)
{
    ToLog("===START COREY_WATCH_DOG==");
    setInterval(show, 35000);
}
